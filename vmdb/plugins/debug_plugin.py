# Copyright 2019  Victor Klos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=



import logging

import cliapp

import vmdb

import json


class DebugPlugin(cliapp.Plugin):

    def enable(self):
        self.app.step_runners.add(DebugStepRunner())


class DebugStepRunner(vmdb.StepRunnerInterface):

    def get_required_keys(self):
        return ['debug']

    def run(self, step, settings, state):
        what = step['debug']
        assert what == 'state'
        vmdb.progress("DEBUG state:")
        for k,v in state.as_dict().items():
            if k == 'tags':
                vmdb.progress("- tags:")
                for tag in v.get_tags():
                    vmdb.progress(f"    {tag}: {v._get(tag)}")
            else:
                vmdb.progress(f"- {k}: {v}")
