# Copyright 2017  Lars Wirzenius
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=



import os
import tempfile

import cliapp

import vmdb

import json


class AnsiblePlugin(cliapp.Plugin):

    def enable(self):
        self.app.step_runners.add(AnsibleStepRunner())


class AnsibleStepRunner(vmdb.StepRunnerInterface):

    def get_required_keys(self):
        return ['ansible', 'playbook']

    def sanitise_tag(self, tag):
        return ''.join([c if c.isalnum() else '_' for c in tag])

    def tags2args(self, tags):
        var = {}
        for tag in tags._tags:
            dev = tags.get_dev(tag)
            mount_point = tags.get_mount_point(tag)
            tag = self.sanitise_tag(tag)
            if dev:
                var[f'vmdb2_dev_{tag}'] = dev
            var[f'vmdb2_ismounted_{tag}'] = mount_point is not None
            if mount_point:
                var[f'vmdb2_mount_point_{tag}'] = mount_point
        return ['--extra-vars', f"\"vars\": [{json.dumps(var)}]"] if var else []

    def run(self, step, settings, state):
        tag = step['ansible']
        playbook = step['playbook']
        mount_point = state.tags.get_mount_point(tag)

        state.ansible_inventory = self.create_inventory(mount_point)
        vmdb.progress(
            'Created {} for Ansible inventory'.format(state.ansible_inventory))

        var_args = self.tags2args(state.tags)

        env = dict(os.environ)
        env['ANSIBLE_NOCOWS'] = '1'
        vmdb.runcmd(
            ['ansible-playbook', '-c', 'chroot'] + var_args +
            ['-i', state.ansible_inventory, playbook],
            env=env)

    def teardown(self, step, settings, state):
        if hasattr(state, 'ansible_inventory'):
            vmdb.progress('Removing {}'.format(state.ansible_inventory))
            os.remove(state.ansible_inventory)

    def create_inventory(self, chroot):
        fd, filename = tempfile.mkstemp()
        os.write(fd, '[image]\n{}\n'.format(chroot).encode())
        os.close(fd)
        return filename
